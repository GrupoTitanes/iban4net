﻿namespace SinKien.IBAN4Net
{
    public interface IAccountManager
    {
        string Create(TitanesAccountType? t);
    }
}