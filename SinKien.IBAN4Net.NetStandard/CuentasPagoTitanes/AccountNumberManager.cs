﻿using System;
using System.Globalization;
using System.Linq;

namespace SinKien.IBAN4Net
{
    public class TitanesAccountNumberManager : IAccountManager
    {
        private string TITANES_BANK_CODE = "6845";
        private string TITANES_OFFICE_CODE = "0001";

        public string Create(TitanesAccountType? t)
        {
            string numeroCuenta = "";

            switch (t)
            {
                case TitanesAccountType.TitanesPOS:
                    numeroCuenta = GetNewAccount();
                    break;

                default:
                    numeroCuenta = GetNewAccount();
                    break;
            }
            //crear la nueva cuenta en registro general
            
            return numeroCuenta;
        }


        public string obtenerDigitoControl(string numeroBase)
        {
            if (numeroBase.Length != 10) throw new Exception();

            int[] multiplicadores = new int[] { 6, 3, 7, 9, 10, 5, 8, 4, 2, 1 };

            char[] digitosNumeroBase = numeroBase.ToCharArray();

            int control = 0;

            for (int i = 1; i <= 10; i++)
            {
                control += int.Parse(digitosNumeroBase[i-1].ToString()) * multiplicadores[10-i];
            }

            control = 11 - (control % 11);
            if (control == 11) control = 0;
            else if (control == 10) control = 1;
            return control.ToString();
        }

        private string GetNewAccount()
        {
            int maxValue = 1000000000;
            Random r = new Random(Seed: (int)(DateTime.Now.Ticks));

            string numeroCuentaGenerado = string.Concat( 
                r.Next(10).ToString("D1"),
                r.Next(1000000000).ToString("D9")
                ).Substring(0,10);

            string cabereraBancoSucursal = string.Concat(TITANES_BANK_CODE, TITANES_OFFICE_CODE);
            //check si cumple
            //check existe sino recursiva hasta que encuentres o 5 llamadas, error no puede crearla

            string digitoControlBanco = obtenerDigitoControl(cabereraBancoSucursal.PadLeft(10, '0'));
            string digitoControlCuenta = obtenerDigitoControl(numeroCuentaGenerado);
            
            return string.Concat(
                cabereraBancoSucursal,
                digitoControlBanco,
                digitoControlCuenta,
                numeroCuentaGenerado
                );
        }
    }
}