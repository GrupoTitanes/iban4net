﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SinKien.IBAN4Net.Exceptions;

namespace SinKien.IBAN4Net.NetStandard.Tests.Net45
{
    [TestClass]
    public class UnitTest1
    {
        private string TITANES_BANK_CODE = "6845";
        private string TITANES_OFFICE_CODE = "0001";

        [TestMethod]
        public void digitoControlCuenta()
        {
            var acManager = new TitanesAccountNumberManager();
            var r = acManager.obtenerDigitoControl("1523085578");

            Assert.AreEqual("3",r);

        }

        [TestMethod]
        public void digitoControlBancoSucursal()
        {
            var acManager = new TitanesAccountNumberManager();
            var r = acManager.obtenerDigitoControl("0068450001");

            Assert.AreEqual("1", r);

        }

        [TestMethod]
        public void IbanConstructionTitanes()
        {
            Iban iban = new IbanBuilder().Build(TitanesAccountType.TitanesPOS);

            //Iban iban2 = new IbanBuilder().CountryCode(CountryCode.GetCountryCode("CZ")).BankCode("0800")
            //    .AccountNumberPrefix("000019").AccountNumber("2000145399").Build();

            Assert.AreEqual("CZ6508000000192000145399", iban.ToString());
        }
    }
}
